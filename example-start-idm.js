module.exports = {
  userName: "String",
  password: "String",
  packageInfo: {
    country: "String",
    originZip: "String",
    destinationZip: "String",
    mailClass: "String"
  },
  accountNumber: "String"
};