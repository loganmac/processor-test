module.exports = {
  "start": {
    "type": "start",
    "idm": "example-start-idm",
    "nextStep": "auth"
  },
  "auth": {
    "type": "service",
    "idm": "example-auth-idm",
    "odm": "example-auth-odm",
    "endPoint": "ms.koloBit.auth/api/authenticate",
    "onError": "Had an authError!",
    "nextStep": "getRate"
  },
  "getRate": {
    "type": "service",
    "idm": "example-rate-idm",
    "odm": "example-rate-odm",
    "endPoint": "ms.koloBit.getRate/api/getRate",
    "onError": "Had a getRateError!",
    "nextStep": "chargeAccount"
  },
  "chargeAccount": {
    "type": "service",
    "idm": "example-charge-idm",
    "odm": "example-charge-odm",
    "endPoint": "ms.koloBit.charge/api/charge",
    "onError": "Had a chargeError!",
    "nextStep": "end"
  },
  "end": {
    "type": "end",
    "idm": "example-end-idm",
    "prevStep": "chargeErrorCheck"
  }
}