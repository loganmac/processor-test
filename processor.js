// var events = require('events');
var exampleWorkFlow = require('./example-workflow');
var request = require('request');

function processEvalLoop(wf, currentStep) {
  step = wf[currentStep]
  console.log(step);
  
  switch(step.type){
    case 'start':
      console.log("Start happening!");
      break;
    case 'end':
      console.log("End happening!");
      break;
    default:
      console.log("Service happening!");
      // mock request here using request package
      console.log("HTTP POST Request made to " + step.endPoint + ".")
      break;
  }
  

  var inboundDataMap = require("./" + step.idm)
  console.log(inboundDataMap);
  if (wf.odm) {outboundDataMap = require("./" + wf.odm); console.log(outboundDataMap)}
  if (step.nextStep) processEvalLoop(wf, step.nextStep)
};

processEvalLoop(exampleWorkFlow, "start");