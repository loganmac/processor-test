module.exports = {
  packageInfo: {
    country: "String",
    originZip: "String",
    destinationZip: "String",
    mailClass: "String"
  }
};